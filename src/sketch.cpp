#include <Arduino.h>
#include <stdint.h>

#define OSC_COUNT 6

struct Oscillator
{
	int8_t midiNote = -1;	//midi note controlling this oscillator.  -1 when not in use.
	uint32_t prevActivationTime = 0;
	uint32_t period = 0;
	uint8_t pinMask;
	bool on = false;
};

Oscillator oscillators[OSC_COUNT];

//store the periods (in uS) for each midi note in a lookup table for fast access.
uint32_t midiPeriodTable[127];

void setup()
{
	//using the regular serial controller.
	//midi baud rate is 31250.
	Serial.begin(9600);

	//calculate midi periods
	for(int i = 0; i < 127; i++)
	{
		//floats are super slow on the atmega328, but it's OK just in the startup routine.
		//in equal temperament, each semitone is 2^1/12 the frequency of the one below it.
		//midi is based on 440 tuning, so a4 (note 69) = 440hz.
		float frequency = pow(2, (i - 69) / 12.0f) * 440;
		midiPeriodTable[i] = (uint32_t) (1000000 / frequency);
	}

	//pinMask stores which pin the oscillator uses for its output on port B.
	//00000001 is pin 8 and 00100000 is pin 13.
	for(int i = 0; i < OSC_COUNT; i++)
		oscillators[i].pinMask = 1 << i;

	//oscillators[0].period = 2273;
	//oscillators[1].period = 1136;

	//set data direction register for pins 8-13 (port B) to output mode.
	DDRB |= B00111111;
}

uint8_t output = 0;
uint8_t command = 0, data1 = 0, data2 = 0;
uint8_t nextOscIndex = 0;
void loop()
{
	uint32_t curTime = micros();

	//command bytes start with 1, data bytes start with 0.
	//this program doesn't differentiate and just assumes the first byte read
	//will be a command, and every third after that.  this seems to be reliable enough for this purpose.
	if(Serial.available() > 2)
	{
		command = Serial.read();
		data1 = Serial.read();
		data2 = Serial.read();

		command >>= 4;	//discard channel info since it isn't used.
		switch(command)
		{
			case 9: //1001XXXX = note on.  data1 = note, data2 = velocity (unused).
				//find an oscillator that's not in use (if there is one)
				//and set it up
				for(uint8_t i = 0; i < OSC_COUNT; i++)
				{
					//use motors in sequential order so all get used evenly
					Oscillator &osc = oscillators[(nextOscIndex + i) % OSC_COUNT];
					nextOscIndex = ++nextOscIndex % OSC_COUNT;

					//use least recently used if there are no motors available
					if(osc.midiNote == -1 || i == OSC_COUNT-1)
					{
						osc.midiNote = data1;
						osc.prevActivationTime = curTime;
						osc.period = midiPeriodTable[data1];
						break;
					}
				}
				break;
			case 8:	//1000XXXX = note off
				//find the oscillator for this note and turn it off
				for(uint8_t i = 0; i < OSC_COUNT; i++)
				{
					Oscillator &osc = oscillators[i];
					if(osc.midiNote == data1)
					{
						osc.midiNote = -1;
						break;
					}
				}
				break;
		}
	}

	uint8_t output = 0;
	for(uint8_t i = 0; i < OSC_COUNT; i++)
	{
		Oscillator &osc = oscillators[i];
		if(osc.midiNote > -1 && curTime - osc.prevActivationTime >= osc.period)
		{
			//osc.on = !osc.on;
			//if(osc.on)
				output |= osc.pinMask;
			//else
			//	output &= (~osc.pinMask);

			//add the period instead of storing the current time
			//since this is usually a little overdue and I don't want that
			//error to accumulate.
			osc.prevActivationTime += osc.period;
		}
	}

	PORTB = output;
}
